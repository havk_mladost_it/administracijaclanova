﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AdministracijaClanova.Migrations
{
    public partial class sitnePromjene4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "IznosClanarine",
                table: "TipoviClanarina",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(5,2)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "IznosClanarine",
                table: "TipoviClanarina",
                type: "decimal(5,2)",
                nullable: false,
                oldClrType: typeof(decimal));
        }
    }
}
