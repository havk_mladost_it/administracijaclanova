﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace AdministracijaClanova.Migrations
{
    public partial class sitnePromjene7 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clanarine_Clan_NositeljGrupneId",
                table: "Clanarine");

            migrationBuilder.DropIndex(
                name: "IX_Clanarine_NositeljGrupneId",
                table: "Clanarine");

            migrationBuilder.CreateIndex(
                name: "IX_Clanarine_ClanId",
                table: "Clanarine",
                column: "ClanId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clanarine_Clan_ClanId",
                table: "Clanarine",
                column: "ClanId",
                principalTable: "Clan",
                principalColumn: "ClanId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Clanarine_Clan_ClanId",
                table: "Clanarine");

            migrationBuilder.DropIndex(
                name: "IX_Clanarine_ClanId",
                table: "Clanarine");

            migrationBuilder.CreateIndex(
                name: "IX_Clanarine_NositeljGrupneId",
                table: "Clanarine",
                column: "NositeljGrupneId");

            migrationBuilder.AddForeignKey(
                name: "FK_Clanarine_Clan_NositeljGrupneId",
                table: "Clanarine",
                column: "NositeljGrupneId",
                principalTable: "Clan",
                principalColumn: "ClanId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
