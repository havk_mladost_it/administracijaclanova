using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdministracijaClanova
{
    [Table("DodatniProizvoljniUnosi")]
    public class DodatniProizvoljniUnosi
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int DodatniProizvoljniUnosiId { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime DatumZapisa { get; set; }

        //[Column(TypeName = "nvarchar(100)")]
        public string Opis { get; set; }

        public int ClanId { get; set; }
        public Clan Clan { get; set; }

    }
}