
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdministracijaClanova
{
    [Table("Treneri")]
    public class Treneri
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TreneriId { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string Ime { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string Prezime { get; set; }

        //[Column(TypeName = "bit")]
        [Required]
        public bool Aktivan { get; set; }

        public int? ClanId { get; set; }
        public Clan Clan { get; set; }

        public ICollection<PripadnostSekciji> PripadnostSekciji { get; set; }

        public ICollection<TreneriDodjele> TreneriDodjele { get; set; }
    }
}