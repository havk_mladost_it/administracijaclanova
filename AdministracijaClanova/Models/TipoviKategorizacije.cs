using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace AdministracijaClanova
{
    [Table("TipoviKategorizacije")]
    public class TipoviKategorizacije
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TipoviKategorizacijeId { get; set; }

        [Column(TypeName = "nvarchar(20)")]
        [Required]
        public string Naziv { get; set; }

        //[Column(TypeName = "bit")]
        [Required]
        public bool Aktivno { get; set; }

        public ICollection<PripadnostSekciji> PripadnostSekcijama { get; set; }
    }
}