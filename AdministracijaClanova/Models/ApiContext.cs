﻿using System;
using Microsoft.EntityFrameworkCore;
namespace AdministracijaClanova
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options) { }
        //osobni podaci
        public DbSet<Clan> Clan { get; set; }
        public DbSet<Skrbnik> PodaciSkrbnika { get; set; }
        //parametarske tablice
        public DbSet<TipoviDuznosti> TipoviDuznosti { get; set; }
        public DbSet<TipoviZapisa> TipoviZapisa { get; set; }
        public DbSet<TipoviClanarina> TipoviClanarina { get; set; }
        public DbSet<TipoviAktivnosti> TipoviAktivnosti { get; set; }
        public DbSet<Sekcija> Sekcija { get; set; }
        public DbSet<TipoviKategorizacije> TipoviKategorizacije { get; set; }
        public DbSet<Karakteristike> Karakteristike { get; set; }
        //transakcijske tablice
        public DbSet<Duznosti> Duznosti { get; set; }
        public DbSet<Clanarine> Clanarine { get; set; }
        public DbSet<Obrazovanje> Obrazovanje { get; set; }
        public DbSet<ZapisiPoClanu> ZapisiPoClanu { get; set; }
        public DbSet<AktivnostiSudjelovanje> AktivnostiSudjelovanje { get; set; }
        public DbSet<DodatniProizvoljniUnosi> DodatniProizvoljniUnosi { get; set; }

        public DbSet<PripadnostSekciji> PripadnostSekciji { get; set; }
        public DbSet<Treneri> Treneri { get; set; }
        public DbSet<TreneriDodjele> TreneriDodjele { get; set; }
        public DbSet<KarakteristikeSportasa> KarakteristikeSportasa { get; set; }

        public DbSet<UplataClanarine> UplataClanarine { get; set; }

        public DbSet<LijecnickiPregledi> LijecnickiPregledi { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Matični podaci
            modelBuilder.Entity<Clan>();
            //.HasKey(bc => new { bc.OIB });

            modelBuilder.Entity<Skrbnik>();
                //.HasKey(bc => new { bc.OIB });
            //parametarske tablice
            modelBuilder.Entity<TipoviDuznosti>();
            modelBuilder.Entity<TipoviZapisa>();
            modelBuilder.Entity<TipoviAktivnosti>();
            modelBuilder.Entity<TipoviClanarina>();
            modelBuilder.Entity<TipoviKategorizacije>();
            modelBuilder.Entity<Karakteristike>();
            modelBuilder.Entity<Sekcija>();
            //Transakcijske tablice
            modelBuilder.Entity<Duznosti>();
            modelBuilder.Entity<Clanarine>();
            modelBuilder.Entity<Obrazovanje>();
            modelBuilder.Entity<ZapisiPoClanu>();
            modelBuilder.Entity<AktivnostiSudjelovanje>();
            modelBuilder.Entity<DodatniProizvoljniUnosi>();
            modelBuilder.Entity<PripadnostSekciji>();
            modelBuilder.Entity<Treneri>();
            modelBuilder.Entity<TreneriDodjele>();
            modelBuilder.Entity<KarakteristikeSportasa>();
            modelBuilder.Entity<UplataClanarine>();
            modelBuilder.Entity<LijecnickiPregledi>();


            //.HasOne(bc => bc.Komponenta)
            //.WithMany(c => c.BrojiloKomponente)
            //.HasForeignKey(bc => bc.KomponentaId)
            //.OnDelete(DeleteBehavior.Restrict);
            //.IsRequired();
        }
    }
}
