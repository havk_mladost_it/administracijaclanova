using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdministracijaClanova
{
    [Table("TipoviZapisa")]
    public class TipoviZapisa
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int TipoviZapisaId { get; set; }

        [Column(TypeName = "nvarchar(5)")]
        [Required]
        public string KodZapisa { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        [Required]
        public string Naziv { get; set; }

        [Column(TypeName = "nvarchar(100)")]
        public string Opis { get; set; }

        //[Column(TypeName = "bit")]
        public bool Aktivan { get; set; }

        public ICollection<ZapisiPoClanu> ZapisiPoClanu { get; set; }
    }
}