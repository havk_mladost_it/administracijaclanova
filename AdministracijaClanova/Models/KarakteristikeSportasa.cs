using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdministracijaClanova
{
    [Table("KarakteristikeSportasa")]
    public class KarakteristikeSportasa
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int KarakteristikeSportasaId { get; set; }

        [Column(TypeName = "decimal")]
        [Required]
        public decimal Vrijednost { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime DatumOpisa { get; set; }

        public int ClanId { get; set; }
        public Clan Clan { get; set; }

        public int KarkateristikeId { get; set; }
        public Karakteristike Karakteristike { get; set; }
    }
}