using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AdministracijaClanova
{
    [Table("AktivnostiSudjelovanje")]
    public class AktivnostiSudjelovanje
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int AktivnostiSudjelovanjeId { get; set; }

        [Column(TypeName = "date")]
        [Required]
        public DateTime DatumAktivnosti { get; set; }

        [Required]
        public int ClanId { get; set; }
        public Clan Clan { get; set; }

        [Required]
        public int TipoviAKtivnostiId { get; set; }
        public TipoviAktivnosti TipoviAktivnosti { get; set; }

    }
}